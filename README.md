# VueGo: Go for Vue.js!
This is a WebAssembly **Vue.js** wrapper written in **Go**.
> We are working hard to relaunch the project with many improvements
> For this reason the code is moving from repository

# How to  Create basic app:
 ### Creating the code for app (Hello VueGo):
 * **Go: (main.go)**
``` go
package main

import (
	"gitlab.com/AndrusGerman/vuego"
)

func  main() {
	// Create Basic app
	app  := vuego.Vue {
		El: "#app",
		Primitive: vuego.Primitive{
			// Defined vars For Vue
			Data: map[string]interface{}{
				"message": "HelloWord VueGo",
			},
		},
	} 
	// Start and Wait
	app.StartWait()
}
```
* **Html: (index.html)**
``` html
<!DOCTYPE  html>
<html  lang="en">
<head>
	<meta  charset="UTF-8">
	<meta  name="viewport"  content="width=device-width, initial-scale=1.0">
	<meta  http-equiv="X-UA-Compatible"  content="ie=edge">
	<title>Hello VueGo</title>
	<!-- VueJs import --> 
	<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
	<!-- Wasm Runtime forGo --> 
	<script src="./wasm_exec.js"></script>
</head>
	<body>
		<!-- Div App Content -->
		<div id="app">
			{{ message }}
		</div>
		<!-- Call VueGo App -->
		<script>
            const go = new Go();
            WebAssembly.instantiateStreaming(fetch('./main.wasm'),go.importObject).then(v =>go.run(v.instance));
        </script>
	</body>
</html>
```

# Import
[New Link](https://gitlab.com/AndrusGerman/vgue)